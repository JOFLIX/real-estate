<?php

get_header();

$show_default_title = get_post_meta(get_the_ID(), '_et_pb_show_title', true);

$is_page_builder_used = et_pb_is_pagebuilder_used(get_the_ID());

?>

    <div id="main-content" class="wish-list-post">
        <?php
        if (et_builder_is_product_tour_enabled()):
            // load fullwidth page in Product Tour mode
            while (have_posts()): the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class('et_pb_post'); ?>>
                    <div class="entry-content">
                        <?php
                        the_content();
                        ?>
                    </div> <!-- .entry-content -->

                </article> <!-- .et_pb_post -->

            <?php endwhile;
        else:
            ?>
            <div class="container">
                <div id="content-area" class="clearfix">
                    <div id="left-area">
                        <?php while (have_posts()) : the_post(); ?>
                            <?php if (et_get_option('divi_integration_single_top') !== '' && et_get_option('divi_integrate_singletop_enable') === 'on') echo et_core_intentionally_unescaped(et_get_option('divi_integration_single_top'), 'html'); ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class('et_pb_post'); ?>>
                                <?php if (('off' !== $show_default_title && $is_page_builder_used) || !$is_page_builder_used) { ?>
                                    <div class="et_post_meta_wrapper">
                                        <h1 class="entry-title"><?php the_title(); ?></h1>

                                        <?php
                                        if (!post_password_required()) :

                                            et_divi_post_meta();

                                            $thumb = '';

                                            $width = (int)apply_filters('et_pb_index_blog_image_width', 1080);

                                            $height = (int)apply_filters('et_pb_index_blog_image_height', 675);
                                            $classtext = 'et_featured_image';
                                            $titletext = get_the_title();
                                            $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                                            $thumb = $thumbnail["thumb"];

                                            $post_format = et_pb_post_format();

                                            if ('video' === $post_format && false !== ($first_video = et_get_first_video())) {
                                                printf(
                                                    '<div class="et_main_video_container">
											%1$s
										</div>',
                                                    et_core_esc_previously($first_video)
                                                );
                                            } else if (!in_array($post_format, array('gallery', 'link', 'quote')) && 'on' === et_get_option('divi_thumbnails', 'on') && '' !== $thumb) {
                                                print_thumbnail($thumb, $thumbnail["use_timthumb"], $titletext, $width, $height);
                                            } else if ('gallery' === $post_format) {
                                                et_pb_gallery_images();
                                            }
                                            ?>

                                            <?php
                                            $text_color_class = et_divi_get_post_text_color();

                                            $inline_style = et_divi_get_post_bg_inline_style();

                                            switch ($post_format) {
                                                case 'audio' :
                                                    $audio_player = et_pb_get_audio_player();

                                                    if ($audio_player) {
                                                        printf(
                                                            '<div class="et_audio_content%1$s"%2$s>
													%3$s
												</div>',
                                                            esc_attr($text_color_class),
                                                            et_core_esc_previously($inline_style),
                                                            et_core_esc_previously($audio_player)
                                                        );
                                                    }

                                                    break;
                                                case 'quote' :
                                                    printf(
                                                        '<div class="et_quote_content%2$s"%3$s>
												%1$s
											</div> <!-- .et_quote_content -->',
                                                        et_core_esc_previously(et_get_blockquote_in_content()),
                                                        esc_attr($text_color_class),
                                                        et_core_esc_previously($inline_style)
                                                    );

                                                    break;
                                                case 'link' :
                                                    printf(
                                                        '<div class="et_link_content%3$s"%4$s>
												<a href="%1$s" class="et_link_main_url">%2$s</a>
											</div> <!-- .et_link_content -->',
                                                        esc_url(et_get_link_url()),
                                                        esc_html(et_get_link_url()),
                                                        esc_attr($text_color_class),
                                                        et_core_esc_previously($inline_style)
                                                    );

                                                    break;
                                            }

                                        endif;
                                        ?>
                                    </div> <!-- .et_post_meta_wrapper -->
                                <?php } ?>

                                <div class="entry-content">
                                    <?php
                                    do_action('et_before_content');

                                    the_content();

                                    wp_link_pages(array('before' => '<div class="page-links">' . esc_html__('Pages:', 'Divi'), 'after' => '</div>'));

                                    //more post
                                    $categories = get_the_category(get_the_ID());
                                    if ($categories) {
                                        echo '<div class="relatedcat et_pb_row et_pb_row_2">';
                                        $category_ids = array();
                                        foreach ($categories as $individual_category) $category_ids[] = $individual_category->term_id;
                                        $args = array(
                                            'category' => $category_ids,
                                            'post__not_in' => array(get_the_ID()),
                                            'posts_per_page' => 3, // So bai viet dc hien thi
                                        );



//                                        $my_query = new wp_query($args);
                                        $post              = get_posts($args);
                                        if ($post):
                                            echo '<h3>TIN GẦN NHẤT</h3>
                                                <div class="et_pb_equal_columns et_pb_gutters2">
                                                    <div class="et_pb_column et_pb_column_4_4 et_pb_column_6    et_pb_css_mix_blend_mode_passthrough et-last-child">
                                                        <div class="et_pb_module et_pb_blog_0 et_pb_blog_grid_wrapper">
                                                            <div class="et_pb_blog_grid clearfix et_pb_bg_layout_light ">
                                                                <div class="et_pb_ajax_pagination_container">
                                                                    <div class="et_pb_salvattore_content" data-columns="3">';
                                            foreach($post as $post) {?>
                                                ?>
                                                <article
                                                    class="et_pb_post clearfix post-1296 post type-post status-publish format-standard has-post-thumbnail hentry category-news">

                                                    <div class="et_pb_image_container"><a
                                                            href="<?php echo($post->post_link) ?>"
                                                            class="entry-featured-image-url">
                                                            <?php echo (get_the_post_thumbnail($post) ? get_the_post_thumbnail($post): "<img src='https://via.placeholder.com/338x190'>")?>

                                                        </a>
                                                    </div> <!-- .et_pb_image_container -->
                                                    <h2 class="entry-title"><a href="<?php echo($post->post_link) ?>" title=""><?php echo($post->post_title) ?></a>
                                                    </h2>

                                                    <p class="post-meta"><span
                                                            class="published">Dec 21, 2018</span>
                                                    </p>
                                                    <div class="post-content"><p>Nulla porttitor accumsan tincidunt.</p>
                                                    </div>
                                                </article>


                                            <?php }
                                        endif;
                                        wp_reset_query();
                                        echo '</div><!-- .et_pb_salvattore_content -->
                                                                </div>
                                                            </div> <!-- .et_pb_posts -->
                                                        </div>
                                                    </div> <!-- .et_pb_column -->
                                                </div>';
                                    }
                                    ?>

                                    <!-- end more post-->

                                </div> <!-- .entry-content -->
                                <div class="et_post_meta_wrapper">
                                    <?php
                                    if (et_get_option('divi_468_enable') === 'on') {
                                        echo '<div class="et-single-post-ad">';
                                        if (et_get_option('divi_468_adsense') !== '') echo et_core_intentionally_unescaped(et_get_option('divi_468_adsense'), 'html');
                                        else { ?>
                                            <a href="<?php echo esc_url(et_get_option('divi_468_url')); ?>"><img
                                                    src="<?php echo esc_attr(et_get_option('divi_468_image')); ?>"
                                                    alt="468" class="foursixeight"/></a>
                                        <?php }
                                        echo '</div> <!-- .et-single-post-ad -->';
                                    }
                                    ?>

                                    <?php if (et_get_option('divi_integration_single_bottom') !== '' && et_get_option('divi_integrate_singlebottom_enable') === 'on') echo et_core_intentionally_unescaped(et_get_option('divi_integration_single_bottom'), 'html'); ?>

                                    <?php
                                    if ((comments_open() || get_comments_number()) && 'on' === et_get_option('divi_show_postcomments', 'on')) {
                                        comments_template('', true);
                                    }
                                    ?>
                                </div> <!-- .et_post_meta_wrapper -->
                            </article> <!-- .et_pb_post -->

                        <?php endwhile; ?>
                    </div> <!-- #left-area -->

                    <?php get_sidebar(); ?>
                </div> <!-- #content-area -->
            </div> <!-- .container -->
        <?php endif; ?>
    </div> <!-- #main-content -->

<?php

get_footer();