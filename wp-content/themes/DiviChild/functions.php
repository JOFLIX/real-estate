<?php
/**
 * @author Divi Space
 * @copyright 2017
 */
if (!defined('ABSPATH')) die();

function ds_ct_enqueue_parent()
{
	wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
}

function ds_ct_loadjs()
{
	wp_enqueue_script(
		'ds-theme-script',
		get_stylesheet_directory_uri() . '/ds-script.js',
		array('jquery')
	);
}

add_action('wp_enqueue_scripts', 'ds_ct_enqueue_parent');
add_action('wp_enqueue_scripts', 'ds_ct_loadjs');

include('login-editor.php');

function divi_child_theme_setup() {
    if ( ! class_exists('ET_Builder_Module') ) {
        return;
    }
    get_template_part( 'custom-modules/cfwpm' );
    $cfwpm = new Custom_ET_Builder_Module_Filterable_Portfolio();
    remove_shortcode( 'et_pb_filterable_portfolio' );
    add_shortcode( 'et_pb_filterable_portfolio', array($cfwpm, '_shortcode_callback') );
}
add_action( 'wp', 'divi_child_theme_setup', 9999 );

?>