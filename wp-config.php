<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mishkac1_forestcity.vn');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@e>Sis;4p&6v8vB4Z7H5T`1uH+QI@JoVal6GJqz8Xk;vV7YV(M90&saCQ}buKD)J');
define('SECURE_AUTH_KEY',  '!KZ0Ga2fP_Kffwl+kBf#p|V-@*@3&_U],`[`;IfR-&nF@m&>UmL+?{3wbss(lY7/');
define('LOGGED_IN_KEY',    'o/m:4g^pd(5Q9]qRk~8QGYraR,)IviDe,_^cx,Z!I78O&l G*t(5L<}j.#]f1})S');
define('NONCE_KEY',        '?c3EGs(Q[0?iBMPgPf5XU1?O`0td45^yj#w??N_UU.Vo3U+,.W#/ge8ig@B;yr]p');
define('AUTH_SALT',        '[]I}iVaI[E3XA!^AM63[rYltH@KUDl(0KehtT>I9)/F%oIoW;_xZucur(::jyYMb');
define('SECURE_AUTH_SALT', '65Ig$lM1&c&l|o/`zq6MB&biBCUaE6g%D94mM-+J`CG7s$|gE`P(6lf4KDs-@gQv');
define('LOGGED_IN_SALT',   '3WZ%=xt}K4yl)~:KHW^yelQ??p#Fmp;vTROAp3K4>9!?P;x9s3/>%E2:bpc{AO;@');
define('NONCE_SALT',       '$F329F}G1TmZWvT,!9$Ys$OBf1xR-<>=xgF~gLDp~Xu%Q-g@I=+_T}%B:(4!8CVl');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
